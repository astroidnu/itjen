<?php
include('../autentifikasi/config.php');
include('../autentifikasi/cek-login.php');
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Edit Tautan</title>
        <!-- Bootstrap -->
       <link href="../../../admin/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../../admin/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="../../../admin/assets/styles.css" rel="stylesheet" media="screen">
        <script src="../../../admin/lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <!-- untuk dtpicker library-->
        <link href="../../../admin/lib/dtpicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">Admin Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo $_SESSION['username'] ?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../../tools/autentifikasi/logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                         <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                            
                                          <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../users/user.php">User</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../berita/berita.php">Berita</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../pengumuman/pengumuman.php">Pengumuman</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../publikasi/publikasi.php">Publikasi</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../galeri/galeri.php">Galeri</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../tautan/tautan.php">Tautan</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../kategori/kategori.php">Kategori</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../marque/marque.php">Marque</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
                 <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="../../index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../users/user.php"><i class="icon-chevron-right"></i>User</a>
                        </li>
                         <li >
                            <a href="../berita/berita.php"><i class="icon-chevron-right"></i>Berita</a>
                        </li>
                        <li>
                            <a href="../pengumuman/pengumuman.php"><i class="icon-chevron-right"></i>Pengumuman</a>
                        </li>
                         <li>
                            <a href="../publikasi/publikasi.php"><i class="icon-chevron-right"></i>Publikasi</a>
                        </li>
						<li>
                            <a href="../agenda/agenda.php"><i class="icon-chevron-right"></i>Agenda</a>
                        </li>
                        <li>
                            <a  href="../galeri/galeri.php"><i class="icon-chevron-right"></i>Galeri</a>
                        </li>
						<li  class="active">
                            <a  href="../tautan/tautan.php"><i class="icon-chevron-right"></i>Tautan</a>
                        </li>
						<li>
                            <a  href="../marque/marque.php"><i class="icon-chevron-right"></i>Marque</a>
                        </li>
                    </ul>
                </div>
                <!--/span-->
                <div class="span9" id="content">
                    <!-- morris graph chart -->
                    <div class="row-fluid section">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Edit Tautan</div>
                            </div>
                            <?php
							$id = $_GET['id'];
							$query = mysql_query("SELECT * FROM tautan WHERE id_tautan='$id'") or die(mysql_error());
							$data = mysql_fetch_array($query);
							?> 
                            <div class="block-content collapse in">
                                <div class="block-content collapse in">
                                <div class="span12">
                                   <form class="form-horizontal" name="input_tautan" enctype="multipart/form-data" method="post" action="update-tautan.php"> 
									<fieldset>
                                        <div class="control-group" for="focusedInput">
                                          <label class="control-label">Nama Tautan </label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="nama_tautan" type="text" placeholder="Nama Tautan " value="<?php echo $data['nama_tautan'];?>">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label">URL </label>
                                          <div class="controls">
                                           <textarea class="input-xlarge textarea" name="url" placeholder="URL ..." style="width: 400px; height: 200px"><?php echo $data['url'];?></textarea>
                                          </div>
                                        </div>
										<div class="control-group" for="focusedInput">
                                          <label class="control-label">Keterangan </label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="ket" type="text" placeholder="keterangan " value="<?php echo $data['keterangan'];?>">
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-danger">Save changes</button>
                                          <button type="reset" class="btn btn-inverse">Reset</button>
                                        </div>
                                      </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
						</div>
                    
            </div>
           <div class="accordion-grup">
          <div class="accordion-inner">
             <p>&copy; Anastasia J3C112058</p>
          </div>
        </div>
        </div>
        <!--/.fluid-container-->
         <script src="../../../admin/lib/js/jquery-1.9.1.min.js"></script>
        <script src="../../../admin/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../admin/assets/scripts.js"></script>
        <!-- js dtpicker -->
        <script src="../../../admin/lib/dtpicker/jquery/jquery-1.8.3.min.js"></script>
        <script src="../../../admin/lib/dtpicker/js/bootstrap-datetimepicker.js"></script>
        <script src="../../../admin/lib/dtpicker/js/locales/bootstrap-datetimepicker.id.js"></script>
        <script type="text/javascript">
           $('.form_date').datetimepicker({
               language:  'id',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0
                  });
        </script>
        
    </body>

</html>