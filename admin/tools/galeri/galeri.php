<?php
include('../autentifikasi/config.php');
include('../autentifikasi/cek-login.php');
?>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Galeri</title>
        <!-- Bootstrap -->
        <link href="../../../admin/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../../admin/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="../../../admin/assets/styles.css" rel="stylesheet" media="screen">
        <script src="../../../admin/lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">Admin Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo $_SESSION['username'] ?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../../tools/autentifikasi/logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                       <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../users/user.php">User</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../berita/berita.php">Berita</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../pengumuman/pengumuman.php">Pengumuman</a>
                                    </li>
                                     <li>
                                        <a tabindex="-1" href="../publikasi/publikasi.php">Publikasi</a>
                                    </li>
                                         <li>
                                        <a tabindex="-1" href="../galeri/galeri.php">Galeri</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
                 <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="../../index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../users/user.php"><i class="icon-chevron-right"></i>User</a>
                        </li>
                         <li >
                            <a href="../berita/berita.php"><i class="icon-chevron-right"></i>Berita</a>
                        </li>
                        <li>
                            <a href="../pengumuman/pengumuman.php"><i class="icon-chevron-right"></i>Pengumuman</a>
                        </li>
                         <li>
                            <a href="../publikasi/publikasi.php"><i class="icon-chevron-right"></i>Publikasi</a>
                        </li>
						<li>
                            <a href="../agenda/agenda.php"><i class="icon-chevron-right"></i>Agenda</a>
                        </li>
                        <li  class="active">
                            <a  href="../galeri/galeri.php"><i class="icon-chevron-right"></i>Galeri</a>
                        </li>
						<li>
                            <a  href="../tautan/tautan.php"><i class="icon-chevron-right"></i>Tautan</a>
                        </li>
						<li>
                            <a  href="../marque/marque.php"><i class="icon-chevron-right"></i>Marque</a>
                        </li>
                    </ul>
				</div>
                
                
                <!--/span-->
                <div class="span9" id="content">
                	<?php
						if (!empty($_GET['message']) && $_GET['message'] == 'success') {
							echo '<div class="alert alert-success">' ;
							echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
							echo '<h4>Success Menambah Gambar</h4>';
							echo '</div>';
						}
						else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
							echo '<div class="alert alert-success">' ;
							echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
							echo '<h4>Success Update Gambar</h4>';
							echo '</div>';
						}
						else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
							echo '<div class="alert alert-success">' ;
							echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
							echo '<h4>Success Delete Gambar</h4>';
							echo '</div>';
						}
						
                  	?>
                    
                  <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Galeri</div>
                            </div>
							<!--tombol search-->
								    <div class="navbar navbar-inner block-header">
								  
								 <div class=" pull-left">
                            	 <a href="tambah-galeri.php" class="btn"><i class="icon-plus"></i> Tambah</a>
								</div>
								 <div class="muted pull-right">
								<fieldset>
								<form class="form-horizontal" name="input_galeri" enctype="multipart/form-data" method="post" action="cari.php"> 
								 <div class="input-group">
								 <input type="text" class="form-control" placeholder="Search for..." name="cari_galeri">
								 <span class="input-group-btn">
								 <button class="btn btn-default" type="submit">Go!</button>
								 </span>
								 </div><!-- /input-group -->
								 </div>
								 </div>
								 </fieldset>
								 </form>
                            <div class="block-content collapse in">
                            	 <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Gambar</th>
												<th>Kategori</th>
												<th>Dibuat Oleh</th>
												<th>Dibuat Tgl</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
											$per_page = 5;
 
											$page_query = mysql_query("SELECT COUNT(*) FROM galeri");
											$pages = ceil(mysql_result($page_query, 0) / $per_page);
 
											$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
											$start = ($page - 1) * $per_page;
											
											$query = mysql_query("SELECT * FROM galeri LIMIT $start, $per_page");
										  
											while ($data = mysql_fetch_array($query)) {
											?>
												<tr>
													<td><?php echo $data['id_gambar']; ?></td>
													<td><?php echo '<img src="'.$data['img_tmp'].'" width = "100" height = "100" />'; ?></td> 
													<td><?php echo $data['kategori'];?></td> 
													<td><?php echo $data['dibuat_oleh']; ?></td>
													<td><?php echo $data['dibuat_tgl']; ?></td>													
                                                    <td> <a href="delete-galeri.php?id=<?php echo $data['id_gambar']; ?>" class="btn" onclick="return confirm('Anda Yakin ?')")><i class="icon-trash"></i>Hapus</a></td>
												</tr>
											<?php	
											}
											?>
                                        </tbody>
                                    </table>
                                    <div align="center">
									<?php
									if($pages >= 1 && $page <= $pages)
									{
									  for($x=1; $x<=$pages; $x++)
									  {
										  //echo ($x == $page) ? '<b><a href="?page='.$x.'">'.$x.'</a></b> ' : '<a href="?page='.$x.'">'.$x.'</a> ';
									  	if($x == $page)
											echo ' <b><a href="?page='.$x.'">'.$x.'</a></b> | ';
										else
											echo ' <a href="?page='.$x.'">'.$x.'</a> |';
									  }
									}
									?>
                                    </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Shop 2013</p>
            </footer>
        </div>
       <!--/.fluid-container-->
        <script src="../../../admin/lib/js/jquery-1.9.1.min.js"></script>
        <script src="../../../admin/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../admin/assets/scripts.js"></script>
    </body>

</html>