<?php
session_start();
 
if (!empty($_SESSION['username'])) {
    header('location:../index.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
	
		<title>Admin Login</title>
		<!-- Bootstrap -->
		<link href="../../../admin/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="../../../admin/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="../../../admin/assets/styles.css" rel="stylesheet" media="screen">
		
		<script src="../../../admin/lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	</head>
	<!-- Styles -->
	<!-- Logo Font - Molle -->
	<link href="css/molle.css" rel="stylesheet" type="text/css" />

	<link href="css/bootstrap.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/icon-style.css" />
	<!--[if lte IE 7]>
	<script src="../scripts/lte-ie7.js"></script>
	<![endif]-->
	<link href="css/bootstrap-responsive.css" rel="stylesheet" />
	<link href="css/radmin.css" rel="stylesheet" id="main-stylesheet" />
	<link href="css/radmin-responsive.css" rel="stylesheet" />

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.cloneposition.js"></script>
	<?php
	//kode php ini kita gunakan untuk menampilkan pesan eror
	if (!empty($_GET['error'])) {
		if ($_GET['error'] == 1) {
			echo '<h3 align="center">Username dan Password belum diisi!</h3>';
		} else if ($_GET['error'] == 2) {
			echo '<h3 align="center">Username belum diisi!</h3>';
		} else if ($_GET['error'] == 3) {
			echo '<h3 align="center">Password belum diisi!</h3>';
		} else if ($_GET['error'] == 4) {
			echo '<h3 align="center">Username dan Password tidak terdaftar!</h3>';
		}
	}
	?>
	
<body id="body-login">

	<div class="container-fluid">

		<div class="row-fluid">
			<div class="span4"></div>
			<div class="span4 login-span">
				<div class="login-radmin align-center">
					<h1 class="brand">
						<span class="rad">ITJEN</span> Kemdikbud
					</h1>
				</div>
			<div class="login-wrapper">
				<div class="login-inner">
					<h2 class="sign-in">Login</h2>
					<small class="muted">Please Login using your registered account details</small>
						<div class="squiggly-border"></div>
						
			<div class="login-inner">
				<form name="login" action="autentifikasi.php" method="post" class="form-horizontal"">
					<div class="input-prepend">
						<span class="add-on"> <i class="radmin-icon radmin-user"></i>
						</span>	
						<input type="text" name="username" class="input-large" id="input-username" size="16" type="text" placeholder="Username"/></div>
			<br />
			<br />
					<div class="input-prepend">
						<span class="add-on"> <i class="radmin-icon radmin-locked"></i>
						</span>
						<input type="password" name="password" class="input-large" id="input-password" size="16" type="text" placeholder="Password"/></div>
						
						<div class="form-actions">
						<input class="btn btn-large btn-inverse pull-right" type="submit" name="login" value="Log in">
						</div>
				 
		 </form>
				</div>
			</div>
		</div>
		</div> <!-- /container -->
	</div>

		<script src="../../../admin/lib/js/jquery-1.9.1.min.js"></script>
		<script src="../../../admin/lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>