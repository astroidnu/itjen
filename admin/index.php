<?php
include('tools/autentifikasi/cek-login.php');
include('tools/autentifikasi/config.php');
error_reporting(0);
if(isset($_SESSION['level']))
{
    if($_SESSION['level'] == "Admin")
    {

    }
    else if($_SESSION['level'] =="User")
    {
        header('location:user/index.php');
    }
}
?>
<!DOCTYPE html>
<html class="no-js">

    <head>
        <title>Dashboard Admin</title>
        <!-- Bootstrap -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="lib/js/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <script src="lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>

    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">Admin Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo $_SESSION['username'] ?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../admin/tools/autentifikasi/logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                         <ul class="nav">
                            <li>
                                <a href="index.php">Dashboard</a>
                            </li>

                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                  <?php
                                  $no=1;
                                    $query1 = mysql_query("select * from menu where (parent = 1 and (position = 1 or position = 3)) order by sort");
                                    while($row = mysql_fetch_array($query1)){ ?>
                                      <li>
                                          <a tabindex="-1" href="tools/<?php echo $row['link'];?>"><?php echo $row['name'];?></a>
                                      </li>
                             <?php } $no++; ?>
                                  </ul>
                            </li>
                            <?php
                            $no=1;
                              $query1 = mysql_query("select * from menu where (parent = 0 and (position = 1 or position = 3)) order by sort");
                              while($row = mysql_fetch_array($query1)){ ?>
                                <li>
                                    <a tabindex="-1" href="tools/<?php echo $row['link'];?>"><?php echo $row['name'];?></a>
                                </li>
                       <?php } $no++; ?>
                        </ul>

                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <?php
                        $no=1;
                          $query1 = mysql_query("select * from menu where (parent = 1 and (position = 2 or position = 3)) order by sort");
                          while($row = mysql_fetch_array($query1)){ ?>
                            <li>
                                <a tabindex="-1" href="tools/<?php echo $row['link'];?>"><?php echo $row['name'];?></a>
                            </li>
                   <?php } $no++; ?>
                    </ul>
                </div>

                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a>
	                                    </li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>

                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Agenda</div>
                                    <?php
                                      $query = mysql_query("select count(jdl_agenda) from agenda");
                                      $agenda_temp = mysql_fetch_array($query);
                                        ?>
                                         <div class="pull-right"><span class="badge badge-info"><?php echo $agenda_temp['count(jdl_agenda)'];?></span>
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                              <th>ID</th>
                                              <th>Judul</th>
                                              <th>Tanggal</th>
                                              <th>Dibuat</th>
                                          </tr>
                                        <?php
                                        $no=1;
                                          $query1 = mysql_query("Select * from agenda order by id_agenda desc limit 5");
                                          while($row = mysql_fetch_array($query1)){
                                      echo  '</thead>';
                                      echo  '<tbody>';
                                            echo'<tr>';
                                            echo'<td>'.$row['id_agenda'].'</td>';
                                            echo'<td>'.$row['jdl_agenda'].'</td>';
                                            echo'<td>'.$row['tgl_mulai'].'</td>';
                                            echo'<td>'.$row['dibuat_oleh'].'</td>';
                                            echo'</tr>';
                                        echo'</tbody>';
                                    }
                                    $no++;
                                        ?>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <?php
                                    $query = mysql_query("select count(judul_berita) from berita");
                                    $berita_temp = mysql_fetch_array($query);
                                 ?>
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Berita</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $berita_temp['count(judul_berita)'];?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Judul Berita</th>
                                                <th>Tanggal Posting</th>
                                                <th>Dipost Oleh</th>
                                            </tr>
                                        </thead>
										<?php
                                        $no=1;
                                          $query1 = mysql_query("Select * from berita");
                                          while($row = mysql_fetch_array($query1)){
                                      echo  '</thead>';
                                      echo  '<tbody>';
                                            echo'<tr>';
                                            echo'<td>'.$row['id_berita'].'</td>';
                                            echo'<td>'.$row['judul_berita'].'</td>';
                                            echo'<td>'.$row['dibuat_tanggal'].'</td>';
                                            echo'<td>'.$row['dibuat_oleh'].'</td>';
                                            echo'</tr>';
                                        echo'</tbody>';
                                         }
                                             $no++;
                                        ?>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                              <?php
                                    $query = mysql_query("select count(jdl_pengumuman) from pengumuman");
                                    $pengumuman_temp = mysql_fetch_array($query);
                                 ?>
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Pengumuman</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $pengumuman_temp['count(jdl_pengumuman)'];?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Judul Pengumuman</th>
                                                <th>Tanggal Posting</th>
                                                <th>Dipost Oleh</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        $no=1;
                                          $query1 = mysql_query("Select * from pengumuman");
                                          while($row = mysql_fetch_array($query1)){
                                      echo  '</thead>';
                                      echo  '<tbody>';
                                            echo'<tr>';
                                            echo'<td>'.$row['id_pengumuman'].'</td>';
                                            echo'<td>'.$row['jdl_pengumuman'].'</td>';
                                            echo'<td>'.$row['tgl_pengumuman'].'</td>';
                                            echo'<td>'.$row['dibuat_oleh'].'</td>';
                                            echo'</tr>';
                                        echo'</tbody>';
                                    }
                                    $no++;
                                        ?>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        <div class="accordion-grup">
          <div class="accordion-inner">
             <p>&copy; Anastasia J3C112058</p>
          </div>
        </div>
        <!--/.fluid-container-->
        <script src="lib/js/jquery-1.9.1.min.js"></script>
        <script src="lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="lib/js/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
    </body>

</html>
