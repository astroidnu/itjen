<?php
session_start();
 
if (!empty($_SESSION['username'])) {
    header('location:../index.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Admin Login</title>
		<!-- Bootstrap -->
		<link href="../../../admin/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="../../../admin/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="../../../admin/assets/styles.css" rel="stylesheet" media="screen">
		
		<script src="../../../admin/lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	</head>
	
	<?php
	//kode php ini kita gunakan untuk menampilkan pesan eror
	if (!empty($_GET['error'])) {
		if ($_GET['error'] == 1) {
			echo '<h3 align="center">Username dan Password belum diisi!</h3>';
		} else if ($_GET['error'] == 2) {
			echo '<h3 align="center">Username belum diisi!</h3>';
		} else if ($_GET['error'] == 3) {
			echo '<h3 align="center">Password belum diisi!</h3>';
		} else if ($_GET['error'] == 4) {
			echo '<h3 align="center">Username dan Password tidak terdaftar!</h3>';
		}
	}
	?>
	
	<body id="login">
		<div class="container">

		  <form name="login" action="autentifikasi.php" method="post" class="form-signin">
			<h2 class="form-signin-heading" align="center">Log in </h2>
			<input type="text" name="username" class="input-block-level" placeholder="Username">
			<input type="password" name="password" class="input-block-level" placeholder="Password">
			<input class="btn btn-large btn-primary" type="submit" name="login" value="Log in">
		  </form>

		</div> <!-- /container -->
		
		<script src="../../../admin/lib/js/jquery-1.9.1.min.js"></script>
		<script src="../../../admin/lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>