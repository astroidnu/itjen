<?php
include('../autentifikasi/config.php');
include('../autentifikasi/cek-login.php');
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>User</title>
        <!-- Bootstrap -->
        <link href="../../../admin/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../../admin/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="../../../admin/assets/styles.css" rel="stylesheet" media="screen">
        <script src="../../../admin/lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">Admin Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo $_SESSION['username'] ?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../../tools/autentifikasi/logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                         <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../users/user.php">User</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="about-us.php">Berita</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="user.php">Pengumuman</a>
                                    </li>
                                     <li>
                                        <a tabindex="-1" href="../publikasi/publikasi.php">Publikasi</a>
                                    </li>

                                         <li>
                                        <a tabindex="-1" href="../galeri/galeri.php">Galeri</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
                 <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="../../index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="../users/user.php"><i class="icon-chevron-right"></i>User</a>
                        </li>
                         <li>
                            <a href="order.php"><i class="icon-chevron-right"></i>Berita</a>
                        </li>
                        <li>
                            <a href="slide.php"><i class="icon-chevron-right"></i>Pengumuman</a>
                        </li>
                        <li>
                            <a href="../publikasi/publikasi.php"><i class="icon-chevron-right"></i>Publikasi</a>
                        </li>
                        <li>
                            <a  href="../galeri/galeri.php"><i class="icon-chevron-right"></i>Galeri</a>
                        </li>
                    </ul>
                </div>
                <!--/span-->
                <div class="span9" id="content">
               
                   <!-- Tambah User -->
                    <div class="row-fluid section">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Tambah User</div>
                            </div>
                            
                            <div class="block-content collapse in">
                                <div class="span12">
                                	 <form class="form-horizontal" name="input_user" method="post" action="insert-user.php"> 
                                      <fieldset>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Username</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="username" type="text" placeholder="username">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label">Password</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="password" type="password" placeholder="password">
                                          </div>
                                        </div>
                                           <div class="control-group">
                                          <label class="control-label" for="focusedInput">Level</label>
                                         <div class="controls">
                                            <select id="select01" class="chzn-select" name="level">
                                              <option>Admin</option>
                                              <option>User</option>
                                            </select>
                                          </div>
                                        </div>
                                     <div class="control-group">
                                          <label class="control-label" for="focusedInput">Dibuat Tanggal</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="dibuat_tanggal" type="text" value="<?php $tgl = date('Y-m-d'); echo $tgl;?>">
                                          </div>
                                        </div>
                               
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                          <button type="reset" class="btn">Reset</button>
                                        </div>
                                      </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                 
                </div>
            </div>
          <div class="accordion-grup">
          <div class="accordion-inner">
             <p>&copy; Personal ICT Project</p>
          </div>
        </div>
        </div>
        <!--/.fluid-container-->
       
        <script src="../../../admin/lib/js/jquery-1.9.1.min.js"></script>
        <script src="../../../admin/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../admin/assets/scripts.js"></script>
    </body>

</html>