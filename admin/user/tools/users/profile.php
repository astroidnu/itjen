<?php
include('../autentifikasi/config.php');
include('../autentifikasi/cek-login.php');
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>User</title>
        <!-- Bootstrap -->
       <link href="../../../user/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../../user/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="../../../user/assets/styles.css" rel="stylesheet" media="screen">
        <script src="../../../user/lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">User Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo $_SESSION['username'] ?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../../tools/autentifikasi/logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../users/user.php">User</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../berita/berita.php">Berita</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../pengumuman/pengumuman.php">Pengumuman</a>
                                    </li>
                                     <li>
                                        <a tabindex="-1" href="../publikasi/publikasi.php">Publikasi</a>
                                    </li>
                                        <li>
                                        <a tabindex="-1" href="../galeri/galeri.php">Galeri</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
                 <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="../../index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="../users/user.php"><i class="icon-chevron-right"></i>User</a>
                        </li>
                         <li >
                            <a href="../berita/berita.php"><i class="icon-chevron-right"></i>Berita</a>
                        </li>
                        <li>
                            <a href="../pengumuman/pengumuman.php"><i class="icon-chevron-right"></i>Pengumuman</a>
                        </li>
                         <li >
                            <a href="../publikasi/publikasi.php"><i class="icon-chevron-right"></i>Publikasi</a>
                        </li>
						<li>
                            <a href="../agenda/agenda.php"><i class="icon-chevron-right"></i>agenda</a>
                        </li>
                        <li>
                            <a  href="../galeri/galeri.php"><i class="icon-chevron-right"></i>Galeri</a>
                        </li>
						<li>
                            <a  href="../tautan/tautan.php"><i class="icon-chevron-right"></i>tautan</a>
                        </li>
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                    <!-- morris graph chart -->
                    <div class="row-fluid section">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Profile</div>
                            </div>
                            <?php
							$id = $_GET['id'];
							$query = mysql_query("select * from users where id='$id'") or die(mysql_error());
							$data = mysql_fetch_array($query);
							?>
                            <div class="block-content collapse in">
                                <div class="span12">
                                	 <form class="form-horizontal" name="edit_user" method="post" action="update-user.php">
                                      <fieldset>
                                       <div class="control-group">
                                          <label class="control-label" for="focusedInput">ID User</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="id" type="text" value="<?php echo $data['id']; ?>" disabled >
                                            <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Username</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="username" type="text" value="<?php echo $data['username']; ?>" >
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label">Password</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="password" type="password" value="<?php echo $data['password']; ?>">
                                          </div>
                                        </div>
                                         <div class="control-group">
                                          <label class="control-label" for="focusedInput">Level</label>
                                          <div class="controls">
                                            <select id="select01" class="chzn-select" name="level">
                                              <?php
                                                //perintah untuk auto select level ketika di edit
                                                 $var = $data['level'];
                                                if ($var == "Admin"){
                                                    echo'<option selected> Admin</option>';
                                                    echo'<option> User</option>';
                                                }else {
                                                    echo'<option> Admin</option>';
                                                    echo'<option selected>User</option>';
                                                    }
                                               ?>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Diedit Tanggal</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="diedit_tgl" type="text" value="<?php echo $data['diedit_tanggal']; ?>">
                                          </div>
                                        </div>
                               
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                          <button type="reset" class="btn">Reset</button>
                                        </div>
                                      </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                      </div> 
                </div>
            </div>
          <div class="accordion-grup">
          <div class="accordion-inner">
             <p>&copy; Personal ICT Project</p>
          </div>
        </div>
        </div>
        <!--/.fluid-container-->
       
        <script src="../../../admin/lib/js/jquery-1.9.1.min.js"></script>
        <script src="../../../admin/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../admin/assets/scripts.js"></script>
    </body>

</html>