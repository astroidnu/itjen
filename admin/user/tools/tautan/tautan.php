<?php
include('../autentifikasi/config.php');
include('../autentifikasi/cek-login.php');
?>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Tautan</title>
        <!-- Bootstrap -->
        <link href="../../../user/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../../user/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="../../../user/assets/styles.css" rel="stylesheet" media="screen">
        <script src="../../../user/lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">Admin Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo $_SESSION['username'] ?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../../tools/autentifikasi/logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                       <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../users/user.php">User</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../berita/berita.php">Berita</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../pengumuman/pengumuman.php">Pengumuman</a>
                                    </li>
                                     <li>
                                        <a tabindex="-1" href="../publikasi/publikasi.php">Publikasi</a>
                                    </li>
									 <li>
                                        <a tabindex="-1" href="../agenda/agenda.php">Agenda</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../galeri/galeri.php">Galeri</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="../tautan/tautan.php">Tautan</a>
                                    </li>
									<li>
										<a tabindex="-1" href="../marque/marque.php">Marque</a>
									</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
                 <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="../../index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../users/user.php"><i class="icon-chevron-right"></i>User</a>
                        </li>
                         <li >
                            <a href="../berita/berita.php"><i class="icon-chevron-right"></i>Berita</a>
                        </li>
                        <li>
                            <a href="../pengumuman/pengumuman.php"><i class="icon-chevron-right"></i>Pengumuman</a>
                        </li>
                         <li >
                            <a href="../publikasi/publikasi.php"><i class="icon-chevron-right"></i>Publikasi</a>
                        </li>
						<li>
                            <a href="../agenda/agenda.php"><i class="icon-chevron-right"></i>Agenda</a>
                        </li>
                        <li>
                            <a  href="../galeri/galeri.php"><i class="icon-chevron-right"></i>Galeri</a>
                        </li>
						<li class="active">
                            <a  href="../tautan/tautan.php"><i class="icon-chevron-right"></i>Tautan</a>
                        </li>
						<li>
							<a  href="../marque/marque.php"><i class="icon-chevron-right"></i>Marque</a>
						</li>
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                	<?php
						if (!empty($_GET['message']) && $_GET['message'] == 'success') {
							echo '<div class="alert alert-success">' ;
							echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
							echo '<h4>Success Menambah Tautan</h4>';
							echo '</div>';
						}
						else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
							echo '<div class="alert alert-success">' ;
							echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
							echo '<h4>Success Update Tautan</h4>';
							echo '</div>';
						}
						else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
							echo '<div class="alert alert-success">' ;
							echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
							echo '<h4>Success Delete Tautan</h4>';
							echo '</div>';
						}
						
                  	?>
                    
                  <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Tautan</div>
                            </div>
							<!--tombol search-->
								   <div class="navbar navbar-inner block-header">
								  
								<div class=" pull-left">
                            	 <a href="tambah-tautan.php" class="btn"><i class="icon-plus"></i> Tambah</a>
								</div>
								<div class="muted pull-right">
								<fieldset>
								<form class="form-horizontal" name="input_tautan" enctype="multipart/form-data" method="post" action="cari.php"> 
								 <div class="input-append">
								 <input type="text" class="form-control" placeholder="Search for..." name="cari_tautan">
								 <span class="input-group-btn">
								 <button class="btn btn-default" type="submit" >Go!</button> 
								 </div>
								 </div>
								 </span>
								 </fieldset>
								 </form>
								 </div>
                            <div class="block-content collapse in">
                            	 <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                
												<th>Nama Tautan</th>
                                                <th>URL</th>
												<th>Ket</th>
                                                <th>Dibuat Oleh</th>
												<th>Dibuat Tgl</th>
												<th>Diedit Oleh</th>
												<th>Diedit Tgl</th>
												
												
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                    $usernamebaru = $_SESSION['username'];

											$per_page = 5;
            
											$page_query = mysql_query("SELECT COUNT(*) FROM tautan  where dibuat_oleh= '$usernamebaru'   order by id_tautan desc");
											$pages = ceil(mysql_result($page_query, 0) / $per_page);
 
											$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
											$start = ($page - 1) * $per_page;
									
											$query = mysql_query("SELECT * FROM tautan where dibuat_oleh= '$usernamebaru' LIMIT $start, $per_page");
										 
											while ($data = mysql_fetch_array($query)) {
                                            ?>
												<tr>
													
													<td><?php echo $data['nama_tautan']; ?></td>
													<td><?php echo $data['url']; ?></td>
													<td><?php echo $data['keterangan']; ?></td>													
                                                    <td><?php echo $data['dibuat_oleh']; ?></td>
													<td><?php echo $data['dibuat_tgl']; ?></td>
													<td><?php echo $data['diedit_oleh']; ?></td>
													<td><?php echo $data['diedit_tgl']; ?></td>
																			
                                                    <td><a href="edit-tautan.php?id=<?php echo $data['id_tautan']; ?>" class="btn">
                                                    <i class="icon-edit"></i></a><br>
													<a href="delete-tautan.php?id=<?php echo $data['id_tautan']; ?>" class="btn" onclick="return confirm('Anda Yakin ?')")><i class="icon-trash"></i></a></td>
												</tr>
											<?php	
											}
											?>
                                        </tbody>
                                    </table>
                                    <div align="center">
									<?php
									if($pages >= 1 && $page <= $pages)
									{
									  for($x=1; $x<=$pages; $x++)
									  {
										  //echo ($x == $page) ? '<b><a href="?page='.$x.'">'.$x.'</a></b> ' : '<a href="?page='.$x.'">'.$x.'</a> ';
									  	if($x == $page)
											echo ' <b><a href="?page='.$x.'">'.$x.'</a></b> | ';
										else
											echo ' <a href="?page='.$x.'">'.$x.'</a> |';
									  }
									}
									?>
                                    </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                </div>
            </div>
                     <div class="accordion-grup">
          <div class="accordion-inner">
             <p>&copy; Anastasia J3C112058</p>
          </div>
        </div>
        </div>
        <!--/.fluid-container-->
        <script src="../../../user/lib/js/jquery-1.9.1.min.js"></script>
        <script src="../../../user/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../user/assets/scripts.js"></script>
    </body>

</html>