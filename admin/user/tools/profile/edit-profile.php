<?php
include('../autentifikasi/config.php');
include('../autentifikasi/cek-login.php');
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Profile</title>
        <!-- Bootstrap -->
        <link href="../../../user/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../../user/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="../../../user/assets/styles.css" rel="stylesheet" media="screen">
        <script src="../../../user/lib/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">User Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo $_SESSION['username'] ?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                         <a tabindex="-1" href="../../../tools/autentifikasi/logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                         <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Manage <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="../profile/profile.php">Profile</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../berita/berita.php">Berita</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../pengumuman/pengumuman.php">Pengumuman</a>
                                    </li>
                                     <li>
                                        <a tabindex="-1" href="../publikasi/publikasi.php">Publikasi</a>
                                    </li>
                  <li>
                                        <a tabindex="-1" href="../agenda/agenda.php">Agenda</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="../galeri/galeri.php">Galeri</a>
                                    </li>
                  <li>
                                        <a tabindex="-1" href="../tautan/tautan.php">Tautan</a>
                                    </li>
                                  
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
                 <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="../../index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                       <li class="active">
                            <a href="../profile/profile.php"><i class="icon-chevron-right"></i>Profile</a>
                        </li>
                         <li >
                            <a href="../berita/berita.php"><i class="icon-chevron-right"></i>Berita</a>
                        </li>
                        <li>
                            <a href="../pengumuman/pengumuman.php"><i class="icon-chevron-right"></i>Pengumuman</a>
                        </li>
                         <li >
                            <a href="../publikasi/publikasi.php"><i class="icon-chevron-right"></i>Publikasi</a>
                        </li>
						<li>
                            <a  href="../agenda/agenda.php"><i class="icon-chevron-right"></i>Agenda</a>
                        </li>
                          <li>
                            <a  href="../galeri/galeri.php"><i class="icon-chevron-right"></i>Galeri</a>
                        </li>
						<li>
                            <a  href="../tautan/tautan.php"><i class="icon-chevron-right"></i>Tautan</a>
                        </li>
						<li>
							<a  href="../marque/marque.php"><i class="icon-chevron-right"></i>Marque</a>
						</li>
                    </ul>
                </div>
                <!--/span-->
                <div class="span9" id="content">
                    <!-- morris graph chart -->
                    <div class="row-fluid section">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Edit Profile</div>
                            </div>
                            <?php
							$username = $_SESSION['id'];
							$query = mysql_query("select * from users where id='$username'") or die(mysql_error());
							$data = mysql_fetch_array($query);
							?>
                            <div class="block-content collapse in">
                                <div class="span12">
                                	 <form class="form-horizontal" name="edit_profile" method="post" action="update-profile.php">
                                      <fieldset>
                                        
                                       <div class="control-group">
                                          <label class="control-label" for="focusedInput">ID User</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="ida" type="text"  value="<?php echo $data['id']; ?>" disabled >
                                              <input class="input-xlarge focused" name="id" type="hidden"  value="<?php echo $data['id']; ?>">
                                          </div>
                                        </div>
                                         <div class="control-group">
                                          <label class="control-label" for="focusedInput">Nama Lengkap</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="fullname" type="text" value="<?php echo $data['nama_lengkap']; ?>" disabled >
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Username</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="username" type="text" value="<?php echo $data['username']; ?>" >
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label">Password</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="password" type="text" value="<?php echo $data['password']; ?>" >
                                          </div>
                                        </div>                  
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-danger">Save Change</button>
                                        </div>
                                      </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                      </div> 
                </div>
            </div>
          <div class="accordion-grup">
          <div class="accordion-inner">
             <p>&copy; Anastasia J3C112058</p>
          </div>
        </div>
        </div>
        <!--/.fluid-container-->
       
        <script src="../../../user/lib/js/jquery-1.9.1.min.js"></script>
        <script src="../../../user/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../user/assets/scripts.js"></script>
    </body>

</html>