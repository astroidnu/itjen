-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Mei 2015 pada 02.55
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `itjen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
`id_agenda` int(11) NOT NULL,
  `jdl_agenda` varchar(30) NOT NULL,
  `isi_agenda` text NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `lokasi` varchar(30) NOT NULL,
  `keterangan` text NOT NULL,
  `dibuat_oleh` varchar(20) NOT NULL,
  `dibuat_tgl` date NOT NULL,
  `diedit_oleh` varchar(20) NOT NULL,
  `diedit_tgl` date NOT NULL,
  `nama_lampiran` varchar(200) NOT NULL,
  `lampiran` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `agenda`
--

INSERT INTO `agenda` (`id_agenda`, `jdl_agenda`, `isi_agenda`, `tgl_mulai`, `tgl_akhir`, `lokasi`, `keterangan`, `dibuat_oleh`, `dibuat_tgl`, `diedit_oleh`, `diedit_tgl`, `nama_lampiran`, `lampiran`) VALUES
(1, 'A', 'a', '2015-05-15', '2015-05-15', 'a', 'a', 'admin', '2015-05-15', '', '0000-00-00', '../../../file/SimController.log', 'SimController.log'),
(2, 'b', 'b', '2015-05-15', '2015-05-15', 'bb', 'b', 'admin', '2015-05-15', '', '0000-00-00', '../../../file/SimController.log', 'SimController.log'),
(3, 'c', 'c', '2015-05-15', '2015-05-15', 'cc', 'c', 'admin', '2015-05-15', '', '0000-00-00', '../../../file/SimController.log', 'SimController.log'),
(4, 'd', 'd', '2015-05-15', '2015-05-15', 'd', 'd', 'admin', '2015-05-15', '', '0000-00-00', '../../../file/SimController.log', 'SimController.log'),
(5, 'e', 'e', '2015-05-15', '2015-05-15', 'e', 'e', 'admin', '2015-05-15', '', '0000-00-00', '../../../file/SimController.log', 'SimController.log'),
(6, 'f', 'f', '2015-05-15', '2015-05-15', 'f', 'f', 'admin', '2015-05-15', '', '0000-00-00', '../../../file/SimController.log', 'SimController.log'),
(7, 'g', 'g', '2015-05-15', '2015-05-15', 'gg', 'g', 'admin', '2015-05-15', '', '0000-00-00', '../../../file/SimController.log', 'SimController.log');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
`id_berita` int(11) NOT NULL,
  `judul_berita` varchar(30) NOT NULL,
  `isi_berita` text NOT NULL,
  `cover_gambar` varchar(3000) NOT NULL,
  `dibuat_oleh` varchar(20) NOT NULL,
  `dibuat_tanggal` date NOT NULL,
  `diedit_oleh` varchar(20) NOT NULL,
  `diedit_tgl` date NOT NULL,
  `kategori` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `img_tmp` varchar(20000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `judul_berita`, `isi_berita`, `cover_gambar`, `dibuat_oleh`, `dibuat_tanggal`, `diedit_oleh`, `diedit_tgl`, `kategori`, `status`, `img_tmp`) VALUES
(3, 'Tes', 'Tes', '../../../admin/gambar/Screenshot_3.jpg', 'admin', '2015-05-14', '', '0000-00-00', 1, 0, '../../../admin/gambar/Screenshot_3.jpg'),
(4, 'A', 'A', '../../../../admin/gambar/Screenshot_2.jpg', 'tasia', '2015-05-15', '', '0000-00-00', 1, 0, '../../../admin/gambar/Screenshot_2.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri`
--

CREATE TABLE IF NOT EXISTS `galeri` (
`id_gambar` int(11) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `kategori` varchar(200) NOT NULL,
  `dibuat_oleh` varchar(200) NOT NULL,
  `dibuat_tgl` date NOT NULL,
  `img_tmp` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `galeri`
--

INSERT INTO `galeri` (`id_gambar`, `gambar`, `kategori`, `dibuat_oleh`, `dibuat_tgl`, `img_tmp`) VALUES
(3, '../../../admin/galeri/Screenshot_4.jpg', 'Pendidikan', 'admin', '2015-05-15', '../../../admin/galeri/Screenshot_4.jpg'),
(4, '../../../../admin/galeri/Screenshot_5.jpg', 'Pendidikan', 'tasia', '2015-05-15', '../../../admin/galeri/Screenshot_5.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`id_kategori` int(11) NOT NULL,
  `jns_kategori` varchar(100) NOT NULL,
  `dibuat_oleh` varchar(50) NOT NULL,
  `dibuat _tgl` date NOT NULL,
  `diedit_oleh` varchar(50) NOT NULL,
  `diedit_tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `marque`
--

CREATE TABLE IF NOT EXISTS `marque` (
`id_marque` int(11) NOT NULL,
  `text_marque` text NOT NULL,
  `dibuat_oleh` varchar(50) NOT NULL,
  `dibuat_tgl` date NOT NULL,
  `diedit_oleh` varchar(50) NOT NULL,
  `diedit_tgl` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `marque`
--

INSERT INTO `marque` (`id_marque`, `text_marque`, `dibuat_oleh`, `dibuat_tgl`, `diedit_oleh`, `diedit_tgl`) VALUES
(4, 'selamat ulang tahun a', 'ibnu', '2015-03-03', 'admin', '2015-04-03'),
(7, 'coba-coba', 'admin', '2015-03-17', 'admin', '2015-03-17'),
(8, 'selamat datang di website itjen kemdikbud jakarta.', 'tasia', '2015-03-23', 'tasia', '2015-03-23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE IF NOT EXISTS `pengumuman` (
`id_pengumuman` int(11) NOT NULL,
  `jdl_pengumuman` varchar(30) NOT NULL,
  `isi_pengumuman` text NOT NULL,
  `tgl_pengumuman` date NOT NULL,
  `dibuat_oleh` varchar(20) NOT NULL,
  `dibuat_tgl` date NOT NULL,
  `diedit_oleh` varchar(20) NOT NULL,
  `diedit_tgl` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `jdl_pengumuman`, `isi_pengumuman`, `tgl_pengumuman`, `dibuat_oleh`, `dibuat_tgl`, `diedit_oleh`, `diedit_tgl`) VALUES
(21, 'kendikbuda', 'inalillahi', '2015-03-24', 'admin', '2015-03-23', 'admin', '2015-04-18'),
(25, 'Test', 'Test2', '2015-04-13', 'tasia', '2015-04-13', 'admin', '2015-04-18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `publikasi`
--

CREATE TABLE IF NOT EXISTS `publikasi` (
`id_pub` int(2) NOT NULL,
  `tgl_pub` date DEFAULT NULL,
  `jdl_pub` varchar(100) DEFAULT NULL,
  `isi_pub` varchar(100) DEFAULT NULL,
  `nama_lampiran` varchar(100) DEFAULT NULL,
  `lampiran` varchar(100) DEFAULT NULL,
  `dibuat_oleh` varchar(100) DEFAULT NULL,
  `dibuat_tgl` date DEFAULT NULL,
  `diedit_oleh` varchar(100) DEFAULT NULL,
  `diedit_tgl` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tautan`
--

CREATE TABLE IF NOT EXISTS `tautan` (
`id_tautan` int(11) NOT NULL,
  `nama_tautan` varchar(30) NOT NULL,
  `url` text NOT NULL,
  `keterangan` text NOT NULL,
  `dibuat_oleh` varchar(20) NOT NULL,
  `dibuat_tgl` date NOT NULL,
  `diedit_oleh` varchar(20000) NOT NULL,
  `diedit_tgl` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tautan`
--

INSERT INTO `tautan` (`id_tautan`, `nama_tautan`, `url`, `keterangan`, `dibuat_oleh`, `dibuat_tgl`, `diedit_oleh`, `diedit_tgl`) VALUES
(3, 'radio itjen kemdikbud', 'radio itjen kemdikbud', 'radioha', 'tasia', '2015-03-17', 'admin', '2015-04-03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(10) NOT NULL,
  `foto` int(11) NOT NULL,
  `level` varchar(5) NOT NULL,
  `dibuat_tgl` date NOT NULL,
  `diedit_tgl` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama_lengkap`, `username`, `password`, `foto`, `level`, `dibuat_tgl`, `diedit_tgl`) VALUES
(1, 'administrator.', 'admin', 'admin', 0, 'Admin', '2015-02-15', '2015-03-04'),
(5, 'asroidnu', 'tasia', 'tasia', 0, 'User', '2015-02-22', '0000-00-00'),
(6, 'tes', 'tes', 'tes123', 0, 'User', '2015-04-13', '2015-04-13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
 ADD PRIMARY KEY (`id_agenda`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
 ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
 ADD PRIMARY KEY (`id_gambar`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `marque`
--
ALTER TABLE `marque`
 ADD PRIMARY KEY (`id_marque`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
 ADD UNIQUE KEY `id_pengunguman` (`id_pengumuman`);

--
-- Indexes for table `publikasi`
--
ALTER TABLE `publikasi`
 ADD UNIQUE KEY `id_pub` (`id_pub`);

--
-- Indexes for table `tautan`
--
ALTER TABLE `tautan`
 ADD PRIMARY KEY (`id_tautan`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
MODIFY `id_agenda` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
MODIFY `id_gambar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marque`
--
ALTER TABLE `marque`
MODIFY `id_marque` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `publikasi`
--
ALTER TABLE `publikasi`
MODIFY `id_pub` int(2) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tautan`
--
ALTER TABLE `tautan`
MODIFY `id_tautan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
